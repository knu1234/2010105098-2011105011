package com.example.app2014;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Chn extends ListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chn);
		loadDB();
	}

	@Override
	public void onResume(){
		super.onResume();
		loadDB();
	}
	
	public void loadDB(){
		SQLiteDatabase db = openOrCreateDatabase(
				"/mnt/sdcard/food.db",
				SQLiteDatabase.CREATE_IF_NECESSARY,
				null
				);
		db.execSQL("CREATE TABLE IF NOT EXISTS restaurant " + 
				"(_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, rank INTEGER, enough INTEGER, type TEXT, rocationX INTEGER, rocationY INTEGER, distance INTEGER);");
		
		Cursor c = db.rawQuery("SELECT _id,name FROM restaurant WHERE type = 'chn' order by distance;",null);
		startManagingCursor(c);
		
		ListAdapter adapt = new SimpleCursorAdapter(
				this,
				android.R.layout.simple_list_item_1,
				c,
				new String[] {"name"},
				new int[] {android.R.id.text1,android.R.id.text2}
				);
		setListAdapter(adapt);
		
		if(db != null){
			db.close();
		}
	}
	
	@Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        
        String x1 = "128.6087486";
        String y1 = "35.8926826";
        String x2 = "128.6100675";
        String y2 = "35.8941476";
        String x3 = "128.605593";
        String y3 = "35.897378";
        String x4 = "128.60645";
        String y4 = "35.8972417";
        //-------------------------------------------------------------
        // 클릭된 항목으로부터 텍스트를 구합니
        
        TextView textViewClicked = (TextView)v;
        String textOfItem = textViewClicked.getText().toString();
        
        //-------------------------------------------------------------
        // 클릭된 항목의 텍스트의 내용에 따라 다른 처리를 수행합니다.
        
        if ("홍콩반점0410".equals(textOfItem)) {
        	
        	Intent intent1 = new Intent(Chn.this,Map.class);
        	intent1.putExtra("input1", x1);
        	intent1.putExtra("input2", y1);
        	intent1.putExtra("input3", "봉구스밥버거");
        	startActivityForResult(intent1,1);
        	finish();
        }
        else if ("더밥스".equals(textOfItem)) {
        	Intent intent1 = new Intent(Chn.this,Map.class);
        	intent1.putExtra("input1", x2);
        	intent1.putExtra("input2", y2);
        	intent1.putExtra("input3", "짚신매운갈비찜");
        	startActivityForResult(intent1,1);
        	finish();
        }
        else if ("짬뽕시대".equals(textOfItem)) {
        	Intent intent1 = new Intent(Chn.this,Map.class);
        	intent1.putExtra("input1", x3);
        	intent1.putExtra("input2", y3);
        	intent1.putExtra("input3", "서울아지매");
        	startActivityForResult(intent1,1);
        	finish();
        }
        else if ("태양반점".equals(textOfItem)) {
        Intent intent1 = new Intent(Chn.this,Map.class);
    	intent1.putExtra("input1", x4);
    	intent1.putExtra("input2", y4);
    	intent1.putExtra("input3", "유림식당");
    	startActivityForResult(intent1,1);
    	finish();
        }
    }
	public void onClickButton(View v){
		int id = v.getId();
		Intent intent1;
		SQLiteDatabase db = openOrCreateDatabase(
				"/mnt/sdcard/food.db",
				SQLiteDatabase.CREATE_IF_NECESSARY,
				null
				);
		
		switch(id){
		case R.id.button1:
			intent1 = new Intent(Chn.this,MenuActivity.class);
			startActivity(intent1);
			finish();
			break;
		case R.id.button2:
			Cursor c = db.rawQuery("SELECT _id,name FROM restaurant WHERE type = 'chn' order by distance;",null);
			startManagingCursor(c);
			
			ListAdapter adapt = new SimpleCursorAdapter(
					this,
					android.R.layout.simple_list_item_1,
					c,
					new String[] {"name"},
					new int[] {android.R.id.text1,android.R.id.text2}
					);
			setListAdapter(adapt);
			break;
		case R.id.button3:
			Cursor c1 = db.rawQuery("SELECT _id,name FROM restaurant WHERE type = 'chn' order by rank;",null);
			startManagingCursor(c1);
			
			ListAdapter adapt1 = new SimpleCursorAdapter(
					this,
					android.R.layout.simple_list_item_1,
					c1,
					new String[] {"name"},
					new int[] {android.R.id.text1,android.R.id.text2}
					);
			setListAdapter(adapt1);
			break;
		case R.id.button4:
			Cursor c2 = db.rawQuery("SELECT _id,name FROM restaurant WHERE type = 'chn' order by enough;",null);
			startManagingCursor(c2);
			
			ListAdapter adapt2 = new SimpleCursorAdapter(
					this,
					android.R.layout.simple_list_item_1,
					c2,
					new String[] {"name"},
					new int[] {android.R.id.text1,android.R.id.text2}
					);
			setListAdapter(adapt2);
			break;			
		}
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.chn, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
