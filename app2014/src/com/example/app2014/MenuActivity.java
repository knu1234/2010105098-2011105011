package com.example.app2014;

import java.sql.Date;

import android.animation.AnimatorSet.Builder;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import java.text.SimpleDateFormat;

import android.app.ActionBar;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

public class MenuActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
	
		//TextView day   = (TextView)findViewById(R.id.ndate);
		TextView clock = (TextView)findViewById(R.id.nclock);
		
		new TimerHandler(clock).start();
	}
	public void onClickButton(View v){
		int id = v.getId();
		Intent intent1;
		
		switch(id){
		case R.id.kor:
			intent1 = new Intent(MenuActivity.this,Kor.class);
			startActivity(intent1);
			break;
		case R.id.jpn:
			intent1 = new Intent(MenuActivity.this,Jpn.class);
			startActivity(intent1);
			break;
		case R.id.chn:
			intent1 = new Intent(MenuActivity.this,Chn.class);
			startActivity(intent1);
			break;
		case R.id.etc:
			intent1 = new Intent(MenuActivity.this,Etc.class);
			startActivity(intent1);
			break;
		}
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	   public void onBackPressed() {
	    String alertTitle = getResources().getString(R.string.app_name);
	    String buttonMessage = getResources().getString(R.string.msg);
	    String buttonYes = getResources().getString(R.string.btn_yes);
	    String buttonNo = getResources().getString(R.string.btn_no);
	     
	    android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
	              builder.setTitle(alertTitle);
	            builder.setMessage(buttonMessage);
	            builder.setNegativeButton(buttonNo, null);
	            builder.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
	                 @Override
	              public void onClick(DialogInterface dialog, int which) {
	               // TODO Auto-generated method stub
	               moveTaskToBack(true);
	               finish();
	              }
	            });
	  
	            builder.show();
	   }
	
}
	